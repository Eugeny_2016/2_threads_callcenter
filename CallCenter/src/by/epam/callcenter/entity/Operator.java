package by.epam.callcenter.entity;

public class Operator {
	private int operatorId;
	
	public Operator(int operatorId) {
		this.operatorId = operatorId;
	}
	
	public int getOperatorId() {
		return operatorId;
	}

}
