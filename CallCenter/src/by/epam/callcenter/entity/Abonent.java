package by.epam.callcenter.entity;

import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

import by.epam.callcenter.controller.CallCenter;
import by.epam.callcenter.exception.CallCenterException;

public class Abonent extends Thread {
	
	private final static Logger LOG = Logger.getLogger(Abonent.class);
	private static final long MAX_WAIT_TIME_MILLIS = 100;
	
	private AtomicBoolean stopThread;
	private CallCenter callCenter;
		
	public Abonent(String name, CallCenter callCenter) {
		super(name);
		this.callCenter = callCenter;
		this.stopThread = new AtomicBoolean(false);
		LOG.debug("Abonent is created!");
	}
	
	public void stopThread() {
		stopThread.set(true);
	}
	
	public void run() {
		try {
			while(!stopThread.get()) {
				sleeping();			// �� ������
				calling();			// ������
			}
		} catch(InterruptedException ex) {
			LOG.info("Abonent has suddenly been interrupted...", ex);
		} catch (CallCenterException ex) {
			LOG.error(ex.getMessage());
		}
	}
	
	private void sleeping() throws InterruptedException {
		Thread.sleep(1000);
	}
	
	private void calling() throws InterruptedException, CallCenterException {
		boolean stopCall = false;
		Operator operator = null;
		Random rand;
		try {
			while (!stopCall) {								// ���� ���� ��������� �������
				LOG.info(getName() + " is calling and trying to get an operator");
				operator = callCenter.getOperator(MAX_WAIT_TIME_MILLIS);	// �������� �������� ���������
				if (operator != null) {
					LOG.info(this.getName() + " has obtained an operator! Now he talks with operator #" + operator.getOperatorId());
					talking();								// ��������
					stopCall = true;						// ����������� ���������
				} else {
					rand = new Random();					// ���� ��������� �������� �� �������, ������� ������: ��������� ��� �������� ������
					int value = rand.nextInt(100);
					if (value % 2 == 0) {
						LOG.info("...for " + getName() + " all operators are busy...and " + getName() + " hungs up the phone!");
						stopCall = true;
					} else {
						LOG.info("...for " + getName() + " all operators are busy...and " + getName() + " decides to wait some time!");
						Thread.sleep(100);
					}
				}
			}
		} finally {						// � ����� ������, ���� ��� ���� ��������, ����� ������� ���������� ��� ������� � �������
			if (operator != null) {
				callCenter.returnOperator(operator);
				LOG.info(getName() + " has stopped talking to operator #" + operator.getOperatorId() + " and returned it back to the queue");
			}
		}
	}
	
	private void talking() throws InterruptedException {
		Random rand = new Random();
		int time = rand.nextInt(200);
		Thread.sleep(time);
	}
}
