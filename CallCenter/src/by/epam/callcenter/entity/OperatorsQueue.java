package by.epam.callcenter.entity;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

public class OperatorsQueue {
	private static final Logger LOG = Logger.getLogger(OperatorsQueue.class);
	
	private Queue<Operator> queue = new LinkedList<Operator>();
	private Lock lock = new ReentrantLock();
	
	public OperatorsQueue(int amountOfOperators) {
		for (int i = 0; i < amountOfOperators; i++) {
			this.queue.add(new Operator(i + 901));
			LOG.debug("Operator is created!");			
		}
		
	}
	
	public Operator getOperator(long maxWaitTime) throws InterruptedException {
		Operator operator = null;
		try {
			lock.tryLock(maxWaitTime, TimeUnit.MILLISECONDS);				// ������� ������������� �������
			operator = queue.poll();										// ���� ������������ �������, �� ����� ����� lock(), �.�. ��� ������ ��� ��������� ������� ����� �������� ��������
																			// ���� ���� ��� ������� �������� ����� ������ �������� �� ������� - ������� � ������!
			
		} finally {						// � ����� ������ ���������� ������������� ������� ���������� ����� ��������� ���������
			lock.unlock();
		}
		return operator;
	}
	
	public boolean putOperator(Operator operator) throws InterruptedException {
		try {
			lock.lock();											// ������� ��������� �������. ���� �� ����������, �� ���, ����� ���������, ��� ������� ��������� � ������� ����� �����������! 
			return queue.offer(operator);							// ������� ����� tryLock() �� �������� � ��������!   
																	// ���� ���� �� ������� �������� ����� ������������ �������� � ������� - ������� � ������!
		} finally {
			lock.unlock();											// � ����� ������ ���������� ������������� ������� ���������� ����� �������� ���������
		}
	}
	
	public Lock getLock() {
		return lock;
	}
}
