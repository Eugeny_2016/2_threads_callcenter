package by.epam.callcenter.exception;

public class CallCenterException extends Exception {

	private static final long serialVersionUID = 1L;

	public CallCenterException() {
		super();
	}

	public CallCenterException(String msg, Throwable ex) {
		super(msg, ex);
	}

	public CallCenterException(String msg) {
		super(msg);
	}

	public CallCenterException(Throwable ex) {
		super(ex);
	}
}
