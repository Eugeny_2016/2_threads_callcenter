package by.epam.callcenter.main;

import org.apache.log4j.Logger;

import by.epam.callcenter.controller.CallCenter;
import by.epam.callcenter.entity.Abonent;

public class Main {

	private static final Logger LOG = Logger.getLogger(Main.class);
	
	public static void main(String[] args) throws InterruptedException {
		LOG.info("Program has started!");
		
		Abonent abonent1 = new Abonent("Abonent #1", CallCenter.getInstance());
		Abonent abonent2 = new Abonent("Abonent #2", CallCenter.getInstance());
		Abonent abonent3 = new Abonent("Abonent #3", CallCenter.getInstance());
		Abonent abonent4 = new Abonent("Abonent #4", CallCenter.getInstance());
		Abonent abonent5 = new Abonent("Abonent #5", CallCenter.getInstance());
		Abonent abonent6 = new Abonent("Abonent #6", CallCenter.getInstance());
		Abonent abonent7 = new Abonent("Abonent #7", CallCenter.getInstance());
		Abonent abonent8 = new Abonent("Abonent #8", CallCenter.getInstance());
		Abonent abonent9 = new Abonent("Abonent #9", CallCenter.getInstance());
		Abonent abonent10 = new Abonent("Abonent #10", CallCenter.getInstance());
		Abonent abonent11 = new Abonent("Abonent #11", CallCenter.getInstance());
		Abonent abonent12 = new Abonent("Abonent #12", CallCenter.getInstance());
		Abonent abonent13 = new Abonent("Abonent #13", CallCenter.getInstance());
		Abonent abonent14 = new Abonent("Abonent #14", CallCenter.getInstance());
		Abonent abonent15 = new Abonent("Abonent #15", CallCenter.getInstance());
		Abonent abonent16 = new Abonent("Abonent #16", CallCenter.getInstance());
		Abonent abonent17 = new Abonent("Abonent #17", CallCenter.getInstance());
		Abonent abonent18 = new Abonent("Abonent #18", CallCenter.getInstance());
		Abonent abonent19 = new Abonent("Abonent #19", CallCenter.getInstance());
		Abonent abonent20 = new Abonent("Abonent #20", CallCenter.getInstance());
		Abonent abonent21 = new Abonent("Abonent #21", CallCenter.getInstance());
		Abonent abonent22 = new Abonent("Abonent #22", CallCenter.getInstance());
		Abonent abonent23 = new Abonent("Abonent #23", CallCenter.getInstance());
		Abonent abonent24 = new Abonent("Abonent #24", CallCenter.getInstance());
		Abonent abonent25 = new Abonent("Abonent #25", CallCenter.getInstance());
		Abonent abonent26 = new Abonent("Abonent #26", CallCenter.getInstance());

		abonent1.start();
		abonent2.start();
		abonent3.start();
		abonent4.start();
		abonent5.start();
		abonent6.start();
		abonent7.start();
		abonent8.start();
		abonent9.start();
		abonent10.start();
		abonent11.start();
		abonent12.start();
		abonent13.start();
		abonent14.start();
		abonent15.start();
		abonent16.start();
		abonent17.start();
		abonent18.start();
		abonent19.start();
		abonent20.start();
		abonent21.start();
		abonent22.start();
		abonent23.start();
		abonent24.start();
		abonent25.start();
		abonent26.start();
		
		Thread.sleep(10000);
		
		abonent1.stopThread();
		abonent2.stopThread();
		abonent3.stopThread();
		abonent4.stopThread();
		abonent5.stopThread();
		abonent6.stopThread();
		abonent7.stopThread();
		abonent8.stopThread();
		abonent9.stopThread();
		abonent10.stopThread();
		abonent11.stopThread();
		abonent12.stopThread();
		abonent13.stopThread();
		abonent14.stopThread();
		abonent15.stopThread();
		abonent16.stopThread();
		abonent17.stopThread();
		abonent18.stopThread();
		abonent19.stopThread();
		abonent20.stopThread();
		abonent21.stopThread();
		abonent22.stopThread();
		abonent23.stopThread();
		abonent24.stopThread();
		abonent25.stopThread();
		abonent26.stopThread();
	}

}
