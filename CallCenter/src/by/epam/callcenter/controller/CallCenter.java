package by.epam.callcenter.controller;

import org.apache.log4j.Logger;

import by.epam.callcenter.entity.Operator;
import by.epam.callcenter.entity.OperatorsQueue;
import by.epam.callcenter.exception.CallCenterException;

public class CallCenter {
	private static final Logger LOG = Logger.getLogger(CallCenter.class);
	private static final int AMOUNT_OF_OPERATORS = 4;
	private static final CallCenter INSTANCE = new CallCenter();			// ����������� ����� �������� �� ��������, => ����� �������� ��� ������ ����������������� ���������
																			// �� ����� ��������������� �������� ����� ���, ����� �������� �������� ��������� ����������� ����-��
	private OperatorsQueue listOfFreeOperators;
	
	private CallCenter() {
		listOfFreeOperators = new OperatorsQueue(AMOUNT_OF_OPERATORS);
		LOG.info("Call-center has been created!");
	}
	
	public static CallCenter getInstance() {
		return INSTANCE;
	}
	// ������������ �������� ����� ����� ��������.
	// ������������ �������, �� ������� ��� ����� ��������: ����� ����� ������ ���� ��������� ����������, 
	// ��������� �������� ���� �� �������� ����������� ������� ���������� � �������� �����. �.�. ����� ����� ���� (���� �������), �������
	// �������� �� ���������� - �� ����������. �� ����� ���� ����� ���� ���������. 
	
	public Operator getOperator(long maxWaitTime) throws InterruptedException {	// ���������� InterruptedException ��� ������ �� ����� �� ������ ������ run()!
		return listOfFreeOperators.getOperator(maxWaitTime);					// ������� �������� ����� ��������� �� �������
	}
	
	public boolean returnOperator(Operator operator) throws InterruptedException, CallCenterException {
		boolean returnResult = false;
		returnResult = listOfFreeOperators.putOperator(operator);					// ������� ���������� ��������� ������� � �������
		if (!returnResult) {														// ���� �� �����-�� (�����������, �� ������-������) �������� ������� ��������� �� �������, ����������� ����������
			throw new CallCenterException("Operator wasn't returned back to the queue!");
		}		
		return returnResult;
	}			
}
